module "s3" {
    source = "./modules/s3"
    bucket_name = "${var.name}-${var.namespace}-bucket"
    tags = {
        Name   = var.name
        Namespace = var.namespace
    }
}  

module "dynamo" {
    source = "./modules/dynamo"
    lock_name         = "${var.name}-locks"
    tags = {
        Name   = var.name
        Namespace = var.namespace
    }
}
module "vpc" {
    private_ips = var.private_ips
    source = "./modules/vpc"
    cidr_block = var.cidr_block
    network = var.network
    tags = {
        Name   = var.name
        Namespace = var.namespace
    }
}


module "ec2" {
    private_ips = var.private_ips
    source = "./modules/ec2"
    subnet_id =  module.vpc.vpc_subnet
    OS = var.OS
    instance_type = var.instance_type
    user_data = "${file("userdata.sh")}"
    tags = {
        Name   = var.name
        Namespace = var.namespace
    }
}