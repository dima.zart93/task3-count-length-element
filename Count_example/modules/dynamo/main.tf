resource "aws_dynamodb_table" "dynamodb" {
  name         = var.lock_name
  billing_mode = var.billing
  hash_key     = var.hash_key
  attribute {
    name = var.hash_key
    type = "S"
  }
  tags = var.tags
}
