variable "cidr_block" {
    description = "Address block for VPC"
    type = string
}

variable "network" {
    description = "Network for instance"
    type = list
}

variable "az" {
    description = "Which region to use"
    type = string
    default = "us-east-1a"
}

variable "public_access"{
    type = string
    default = true
}

variable "private_ips" {
    type = list
}

variable "tags" {
  description = "Tags to set on the VPC"
  type        = map(string)
  default     = {}
}

