variable "bucket_name" {
  description = "Name for bucket"
  type = string
}
variable "tags" {
  description = "Tags to set on the bucket."
  type        = map(string)
  default     = {}
}