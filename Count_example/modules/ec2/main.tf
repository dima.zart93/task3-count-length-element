resource "aws_instance" "ec2" {
  count = length(var.private_ips)
  ami           = var.OS 
  instance_type = var.instance_type
  network_interface {
    network_interface_id = aws_network_interface.ec2_nic[count.index].id
    device_index         = 0
  } 
  key_name = var.key_name

  user_data = var.user_data

  tags = var.tags
}

resource "aws_network_interface" "ec2_nic" {
  count = length(var.private_ips)
  subnet_id   = element(var.subnet_id,count.index)

  tags = var.tags
}