variable "profile" {
    description = "Which profile to use"
    type = string
    default = "mycloud"
}

variable "name"{
    description = "Common name of Task"
    type = string
    default = "task2"
}

variable "namespace" {
    description = "Common namespace"
    type = string
    default = "tsart"
}

variable "region" {
    description = "Region for bucket"
    type = string
    default = "us-east-1"
}

variable "cidr_block" {
    description = "Address block for VPC"
    type = string
}

variable "network" {
    description = "Network for instance"
    type = list
}

variable "key" {  
    description = "Which key macth tfstate"
    type = string
    default = "terraform.tfstate"
}

variable "OS" {
    description = "Which AMI to use"
    type = string
    default = "ami-04505e74c0741db8d"
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "az" {
    description = "Which region to use"
    type = string
    default = "us-east-1a"
}

variable "public_access"{
    type = string
    default = true
}

variable "private_ips" {
    type = list
}
