output "vpc_id" {
  description = "Name (id) of the VPC"
  value       = aws_vpc.vpc.id
}

output "vpc_subnet" {
    value       = [ for subnet_id in aws_subnet.vpc_subnet : subnet_id]
}

