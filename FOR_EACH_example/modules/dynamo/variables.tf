variable "lock_name" {
    description = "Name for DynamoDB"
    type = string
}
variable "hash_key" {
    description = "Hash-key for DynamoDB"
    type = string
    default = "LockID"
}

variable "billing" {
    type = string
    default = "PAY_PER_REQUEST"  
}

variable "tags" {
  description = "Tags to set on the DynamoDB."
  type        = map(string)
  default     = {}
}