variable "user_data" {
  description = "File with userdata"
  type = string  
}

variable "key_name" {
    description = "Key pair"
    type = string
    default = "Test"
}

variable "tags" {
  description = "Tags to set on the EC2-instance."
  type        = map(string)
  default     = {}
}

variable "OS" {
  type = string  
}

variable "instance_type" {
  type = string
}

variable "subnet_id" {
  type = string
}
/*
variable "private_ips" {
    type = list
}
*/
variable "network" {
    description = "Network for instance"
    type = map
}