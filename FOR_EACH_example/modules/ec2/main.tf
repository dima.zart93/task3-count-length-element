resource "aws_instance" "ec2" {
  ami           = var.OS 
  instance_type = var.instance_type
  for_each = var.network
  #private_ips = var.private_ips
  /*
  network_interface {
    network_interface_id = aws_network_interface.ec2_nic[each.key].id
    device_index         = 0
  } 
  */
  key_name = var.key_name

  user_data = var.user_data

  tags = var.tags
}
/*
resource "aws_network_interface" "ec2_nic" {
  for_each = var.network
  subnet_id   = var.subnet_id

  tags = var.tags
}
*/