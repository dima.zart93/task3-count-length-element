output "name" {
  description = "Name (id) of the EC2-instance"
  value       = [ for instance in aws_instance.ec2 : instance.id]
}

output "ip" {
  description = "Public IP of the EC2-instance"
  value       = [ for network_interface_id in aws_instance.ec2 : network_interface_id.public_ip]
}

output "key" {
  description = "Key for EC2-instance"
  value       = [ for key_name in aws_instance.ec2 : key_name.id]
}

output "ec2_nic" {
    value = [ for network_interface_id in aws_instance.ec2 : network_interface_id.id]   
}