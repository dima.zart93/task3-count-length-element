output "arn" {
  description = "ARN of the bucket"
  value       = aws_s3_bucket.s3.arn
}

output "name" {
  description = "Name (id) of the bucket"
  value       = aws_s3_bucket.s3.id
}

