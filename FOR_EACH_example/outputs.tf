output "vpc-id" {
    value = module.vpc.vpc_id
}

output "ec2-ip" {
    value = module.ec2.ip
}
