/*
terraform {
  backend "s3" {
    profile = "mycloud"
    # Replace this with your bucket name!
    bucket         = "tsart-task1-state"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    # Replace this with your DynamoDB table name!
    dynamodb_table = "task1-locks"
    encrypt        = true
  }
}

*/
provider "aws" {
  profile = var.profile
  region = var.region
}