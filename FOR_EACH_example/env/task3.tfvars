name = "task3"
namespace = "tsart"
cidr_block = "172.16.0.0/16"
network = {
      one = "172.16.1.0/24",
      two = "172.16.2.0/24"
    }
private_ips = ["172.16.1.77","172.16.2.77"]
public_access = true